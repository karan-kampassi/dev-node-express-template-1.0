const express = require('express');

const bodyparser = require('body-parser')

const app = express();

app.use(bodyparser.urlencoded({extended:false}));

app.get('/',(request,response)=>{
    response.send('Hello, Welcome to Engineering Lab! Start editing to see some magic happen :)');
});

const server = app.listen(4000);

module.exports = server;